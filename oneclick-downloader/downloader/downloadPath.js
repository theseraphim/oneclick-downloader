function downloadPath() {

    chrome.storage.sync.get({
        'downloadPathOption': 'none',
        'downloadCustomPath': ''
    }, function(items) {

        var dlpath = '';
        var dlpathLabel = document.getElementById('downloadPathLabel');
        var dlpathPath = document.getElementById('downloadPath')
        var dlpathSuffix = document.getElementById('downloadPathSuffix');

        switch (items.downloadPathOption) {
            case 'custom':
                dlpath = items.downloadCustomPath;
                dlpathPath.disabled = 'disabled';
                break;
            case 'none':
                dlpath = null;
                dlpathPath.style.display = 'none';
                dlpathSuffix.style.display = 'none';
                break;
            case 'request':
                dlpath = '';
                break;
        }

        document.getElementById('downloadPath').value = dlpath;

    });

}


// > var s = 'http://www.example.com/foo/bar/baz.jpg';
// > s.getLast();
// = 'baz.jpg'
String.prototype.getLast = function() {
    return this.substr(this.lastIndexOf('/') + 1);
};


// > var h = 'http://www.example.com/foo/bar/baz.jpg'
// > h.getHost();
// = 'example.com'
String.prototype.getHost = function() {
    var parser = document.createElement('a');
    var hostname = '';
    parser.href = this;
    hostname = parser.hostname.replace('www.', '').split('.').reverse();
    return hostname[1]+'.'+hostname[0];
}


// > var h = 'Hello World!'
// > h.startsWith('Hel');
// = true
// > h.startsWith('blah');
// = false
String.prototype.startsWith = function(check) {
    return this.slice(0, check.length) == check;
}


// > var n = 'Image%20%281%29.jpg'
// > n.decode()
// = 'Image (1).jpg'
String.prototype.decode = function() {
    return decodeURI(this);
}

// Lots of hosters use the Open Graph Protocol and thus have og:image
// containing the full size image. This function can be used to get this image
// using one single function in the hoster’s function
//
// function myHoster_function(c) {
//     return use_og_image(c);
// }
//
// … that’s it.
function return_og_image(c) {
    var img = c.querySelectorAll('[property="og:image"]')[0].content
    return {
        'url': img,
        'name': img.getLast()
    }
}

// > var decoded = b.bToObj()
// = '<body><a href="...">...</a></body>'
String.prototype.bToObj =  function() {
    var decoded = window.atob(this);
    var parser = new DOMParser();
    return parser.parseFromString(decoded, 'text/html').body;
}
